/*
remove yterminate call from scanner and fixed it so that 
it insists on spaces between tokens
[[:alpha:]]+  Alphabetic characters regex same as [A-Za-z]..
 FLEX OPTIONS
*/
%option noinput nounput noyywrap yylineno nodefault
%{
  #include "jazyk.tab.h"
%}
%%
[[:space:]]+
[Bb][Ee][Gg][Ii][Nn]    return(LBEGIN);
[Ee][Nn][Dd]            return(LEND);
[Ii][Ff]                return(LIF);
[Tt][Hh][Ee][Nn]        return(LTHEN);
[Pp]                    return(LP);
[Vv]                    return(LV);
[[:alpha:]]+            { printf("Unknown token: %s\n", yytext); }
[.;]                    return(*yytext);
.                       { printf("unknown character in input: %c\n", *yytext);}
