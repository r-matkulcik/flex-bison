CC = gcc 
CFLAGS = -g -O 
YACC = bison 
YFLAGS = -d -v
LEX = flex 
LFLAGS = 
SRC = jazyk.y jazyk.l 
OBJ = jazyk.tab.o lex.yy.o 
all: jazyk 

jazyk: $(OBJ) 
	$(CC) $(CFLAGS) $(OBJ) -o $@ -lm -ly -lfl

jazyk.tab.o: jazyk.tab.c

lex.yy.c: jazyk.l 
	$(LEX) $(LFLAGS) jazyk.l

jazyk.tab.c: jazyk.y 
	$(YACC) $(YFLAGS) jazyk.y

clean:; @rm -f jazyk jazyk.output jazyk.tab.h jazyk.tab.c lex.yy.c *.o 
