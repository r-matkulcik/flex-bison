%{
  #include <stdio.h>
  typedef struct statistics {
    int if_count;
    int max_depth;
  } statistics;
  statistics merge_statistics(statistics a, statistics b) {
    return (statistics){a.if_count + b.if_count,
                        a.max_depth > b.max_depth ? a.max_depth : b.max_depth};
  }

  #define YYSTYPE statistics
  extern int yylineno;
  int yylex();
  void yyerror(const char* message);
%}

// define the "terminal symbol" token types (in CAPS by convention)
%token LIF "if" LTHEN "then" LBEGIN "begin" LEND "end"
%token LV
%token LP                       
%start program                  

// added a syntax for if statements with blocks because it was trivial and it makes the program a bit more interesting. When the parser adds a
//statement to a block, it needs to sum the current if count for the block and the if count for the new statement, and compute the maximum depth as
//the maximum of the two depths. The function merge_statistics does that.
%%

program: block '.'                    { printf("If count (Počet príkazov if): %d\n" 
						"max depth (Maximálna hĺbka vnorenia príkazu p): %d\n",
                                               $1.if_count, $1.max_depth); }
block: "begin" statements "end"       { $$ = $2; }
statements: /* empty */               { $$ = (statistics){0, 0}; }
          | statements statement ';'  { $$ = merge_statistics($1, $2); }
statement : LP                        { $$ = (statistics){0, 1}; }
          | block
          | "if" LV "then" statement  { $$ = (statistics){$4.if_count + 1,
                                                          $4.max_depth + 1}; }
%%

void yyerror(const char* message) {
  printf("At %d: %s\n", yylineno, message);
}



int main(int argc, char** argv) {
    printf("Examples to better copy in console: \n");
    printf("begin p; p; end. \n");
    printf("begin if v then p; end.\n");
    printf("begin p; if v then if v then p; p; end.\n");
    printf("begin if v then begin if v then p; if v then p; end; end.\n");
    printf("\n");

    int status = yyparse();
    /* Unnecessary because yyerror will print an error message */
    if (status != 0) printf("Parse failed (Vyraz nebol syntakticky spravny)\n");
    return status;
 
}
